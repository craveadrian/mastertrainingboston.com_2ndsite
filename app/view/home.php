<div id="section1">
	<div class="row">
		<h2>Results for every. Body.</h2>
		<div class="wrapper">
			<dl>
				<dt> <img src="public/images/content/result1.png" alt="Result Image 1"> </dt>
				<dd>Creative Athletic <span>Training</span></dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/result2.png" alt="Result Image 2"> </dt>
				<dd>Training Muscle <span>Endurance</span></dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/result3.png" alt="Result Image 3"> </dt>
				<dd>Personal & Group <span>Training</span></dd>
			</dl>
		</div>
	</div>
</div>
<div id="section2">
	<div class="row">
		<div class="wrapper">
			<div class="sec2Left">
				<div class="text">
					<h2>Master Training</h2>
					<p>The Best Fitness in Boston, MA</p>
					<p>Are you looking to improve your personal fitness and really get in shape? We have exceptional facilities, as well as offering great personal training services that are specifically tailored to suit your personal needs and goals. We are also a 24/7 gym so that everyone can fit a little workout into their schedule.</p>
					<p>Come down to Master Training today and get started right now! </p>
				</div>
				<a href="services#content" class="btn">learn more</a>
			</div>
			<div class="sec2Right">
				<img src="public/images/content/section2IMG.jpg" alt="Weight Lifting Man">
			</div>
		</div>
	</div>
</div>
<div id="section3">
	<div class="row">
		<h2>get fit your way</h2>
		<p>GET FITTER FASTER WITH MASTER TRAINING</p>
		<div class="container">
			<dl>
				<dt> <img src="public/images/content/fit1.png" alt="clock"> </dt>
				<dd>24 HOUR ACCESS</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/fit2.png" alt="walking"> </dt>
				<dd>CARDIO EQUIPMENT</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/fit3.png" alt="weights"> </dt>
				<dd>FREE WEIGHTS</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/fit4.png" alt="trainer"> </dt>
				<dd>PERSONAL TRAINERS</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/fit5.png" alt="strength"> </dt>
				<dd>STRENGTH TRAINING</dd>
			</dl>
		</div>
		<img src="public/images/content/round.png" alt="button" class="rbuttons">
	</div>
</div>
<div id="section4">
	<div class="row">
		<h2>SUCCESS STORIES</h2>
		<p class="desc">Get the best out of yourself and reach your goals faster!</p>
		<div class="container1">
			<dl>
				<dt> <img src="public/images/content/story1.jpg" alt="Scott"> </dt>
				<dd>
					<h4>SCOTT | MASTER TRAINING</h4>
					<p>Four years ago I was in the worst shape of my life weighing 230 pounds...</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/story2.jpg" alt="Jennifer"> </dt>
				<dd>
					<h4>JENNIFER | MASTER TRAINING</h4>
					<p>In August of 2008 I was diagnosed with Hodgkin’s Lymphoma, a form of cancer. ...</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/story3.jpg" alt="Becky"> </dt>
				<dd>
					<h4>BECKY | MASTER TRAINING</h4>
					<p>When I was 35 years old and 16 weeks pregnant, I was in a severe...</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/story4.jpg" alt="Steve"> </dt>
				<dd>
					<h4>STEVE | MASTER TRAINING</h4>
					<p>During my childhood, I was always a small and very skinny guy.  I could eat...</p>
				</dd>
			</dl>
		</div>
		<a href="services#content" class="btn">READ MORE</a>
		<div class="container2">
			<img src="public/images/content/story5.jpg" alt="weight lifting">
			<img src="public/images/content/story6.jpg" alt="yoga">
			<img src="public/images/content/story7.jpg" alt="working out lady">
		</div>
	</div>
</div>
<div id="section5">
	<div class="row">
		<h2>MEET OUR trainers</h2>
		<div class="container">
			<dl>
				<dt> <img src="public/images/content/trainer1.jpg" alt="Greg"> </dt>
				<dd>
					<h4>GREG ROBERT</h4>
					<p>MANAGER</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/trainer2.jpg" alt="Benny"> </dt>
				<dd>
					<h4>BENNY CARSON</h4>
					<p>PERSONAL TRAINER</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/trainer3.jpg" alt="Anthony"> </dt>
				<dd>
					<h4>ANTHONY WALLER</h4>
					<p>PERSONAL TRAINER</p>
				</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/trainer4.jpg" alt="Jimmy"> </dt>
				<dd>
					<h4>JIMMY BARNES</h4>
					<p>PERSONAL TRAINER</p>
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="section6">
	<div class="row">
		<h2>our gallery</h2>
		<p>Whether you’re a weekend warrior, professional athlete or something in between, our master training room and world-class gym will help you turbocharge your athletic performance.</p>
		<div class="container">
			<img src="public/images/content/gallery1.jpg" alt="Gallery Image 1">
			<img src="public/images/content/gallery2.jpg" alt="Gallery Image 2">
			<img src="public/images/content/gallery3.jpg" alt="Gallery Image 3">
			<img src="public/images/content/gallery4.jpg" alt="Gallery Image 4">
			<img src="public/images/content/gallery5.jpg" alt="Gallery Image 5">
			<img src="public/images/content/gallery6.jpg" alt="Gallery Image 6">
			<img src="public/images/content/gallery7.jpg" alt="Gallery Image 7">
			<img src="public/images/content/gallery8.jpg" alt="Gallery Image 8">
			<img src="public/images/content/gallery9.jpg" alt="Gallery Image 9">
			<img src="public/images/content/gallery10.jpg" alt="Gallery Image 10">
		</div>
		<a href="gallery#content" class="btn">VIEW MORE</a>
	</div>
</div>
<div id="section7"></div>
<div id="section8">
	<div class="row">
		<div class="container">
			<h2>Contact Us</h2>
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
				<label><span class="ctc-hide">Message</span>
					<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
				</label>
				<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
				<div class="g-recaptcha"></div>
				<label>
					<input type="checkbox" name="consent" class="consentBox"> I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
				</label><br>
				<?php if( $this->siteInfo['policy_link'] ): ?>
				<label>
					<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
				</label>
				<?php endif ?>
				<button type="submit" class="ctcBtn btn" disabled>Submit form</button>
			</form>
		</div>
		<div class="sec8Left fl">
			<h3>quick links</h3>
			<ul>
				<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
				<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
				<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
				<li <?php $this->helpers->isActiveMenu("testimonials"); ?>><a href="<?php echo URL ?>testimonials#content">TESTIMONIALS</a></li>
				<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
				<li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy#content">PRIVACY POLICY</a></li>
			</ul>
		</div>
		<div class="sec8Right fr">
			<h3>contact us</h3>
			<p>
				PHONE:
				<span><?php $this->info(["phone","tel"]); ?></span>
			</p>
			<p>
				EMAIL
				<span><?php $this->info(["email","mailto"]); ?></span>
			</p>
			<p>
				WE ACCEPT:
				<span> <img src="public/images/content/cards.png" alt="cards"> </span>
			</p>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
