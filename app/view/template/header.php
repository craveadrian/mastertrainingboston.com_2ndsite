<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="wrapper">
					<div class="hdLeft">
						<a href="<?php echo URL; ?>">
							<img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name") ?>  logo" class="headerLogo">
						</a>
					</div>
					<div class="hdRight">
						<div class="hdRightTop">
							<p class="sm">
								<a href="<?php $this->info("fb_link"); ?>" target="_blank">f</a>
								<a href="<?php $this->info("tt_link"); ?>" target="_blank">l</a>
								<a href="<?php $this->info("li_link"); ?>" target="_blank">i</a>
								<a href="<?php $this->info("gp_link"); ?>" target="_blank">g</a>
							</p>
							<p><?php $this->info(["email","mailto","hdEmail"]); ?></p>
							<p><?php $this->info(["phone","tel","hdPhone"]); ?></p>
						</div>
						<div class="hdRightBot">
							<nav id="navbar">
								<a href="#" id="pull"><strong>MENU</strong></a>
								<ul>
									<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
									<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
									<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
									<li <?php $this->helpers->isActiveMenu("testimonials"); ?>><a href="<?php echo URL ?>testimonials#content">TESTIMONIALS</a></li>
									<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
									<li <?php $this->helpers->isActiveMenu("privacy-policy"); ?>><a href="<?php echo URL ?>privacy-policy#content">PRIVACY POLICY</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<img src="public/images/common/bgBanner.jpg" alt="Banner 1">
			<div class="row">
				<div class="text">
					<h1>Get Fit When It Fits </h1>
					<p>OPEN 24/7 TO MEMBERS</p>
					<a href="contact#content" class="bnBtn">FREE 1 WEEK TRIAL FOR ALL<br/>NEW MEMBERS</a>
				</div>
				<div class="buttons">
					<a href="#">&bull;</a>
					<a href="#">&bull;</a>
					<a href="#">&bull;</a>
					<a href="#">&bull;</a>
					<a href="#">&bull;</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
